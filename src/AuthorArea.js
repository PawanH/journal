import React from "react";
import { Box, Text, TextArea, Button } from "grommet";

export const AuthorArea = function({
  coords,
  time,
  handlePublish,
  handleTextChange,
  handleReset,
  content
}) {
  return (
    <Box gap="medium" fill>
      <Box pad="small">
        <Box>
          <LocationWidget coords={coords} />
        </Box>
        <Box>
          <Text>
            <span role="img" aria-label="clock">
              ⏰
            </span>{" "}
            {time.toTimeString()}
          </Text>
        </Box>
      </Box>
      <Box flex="grow" pad="small">
        <TextArea
          placeholder="What is on your mind?"
          value={content}
          onChange={handleTextChange}
          fill
        />
      </Box>
      <Box direction="row" gap="small" margin="small">
        <Button label="Reset" type="reset" onClick={handleReset} />
        <Button
          label="Publish"
          type="submit"
          onClick={() => {
            handlePublish(content, null, null, {
              latitude: coords.latitude,
              longitude: coords.longitude
            });
          }}
          primary
        />
      </Box>
    </Box>
  );
};

const LocationWidget = function({ coords }) {
  if (!coords.latitude || !coords.longitude) {
    return (
      <Text>
        <span role="img" aria-label="pushpin">
          📍
        </span>{" "}
        Location unavailable
      </Text>
    );
  }

  return (
    <Text>
      <span role="img" aria-label="pushpin">
        📍
      </span>{" "}
      {coords.latitude}, {coords.longitude} (accurate to {coords.accuracy}{" "}
      metres }
    </Text>
  );
};
