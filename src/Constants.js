export const API_KEY = process.env.REACT_APP_API_KEY;
export const CLIENT_ID = process.env.REACT_APP_CLIENT_ID;
export const REDIRECT_URI = process.env.REACT_APP_REDIRECT_URI;

export const SCOPE = "https://www.googleapis.com/auth/drive.file";
export const OAUTH2_ENDPOINT = "https://accounts.google.com/o/oauth2/v2/auth";
export const FOLDER_MIME_TYPE = "application/vnd.google-apps.folder";

export const THEME = {
  name: "Journal Theme",
  rounding: 4,
  spacing: 24,
  defaultMode: "dark",
  global: {
    colors: {
      brand: "royalblue",
      background: {
        dark: "#222222",
        light: "#FFFFFF"
      },
      "background-strong": {
        dark: "#000000",
        light: "#FFFFFF"
      },
      "background-weak": {
        dark: "#444444a0",
        light: "#E8E8E880"
      },
      "background-xweak": {
        dark: "#66666699",
        light: "#CCCCCC90"
      },
      text: {
        dark: "#EEEEEE",
        light: "#333333"
      },
      "text-strong": {
        dark: "#FFFFFF",
        light: "#000000"
      },
      "text-weak": {
        dark: "#CCCCCC",
        light: "#444444"
      },
      "text-xweak": {
        dark: "#999999",
        light: "#666666"
      },
      border: "background-xweak",
      control: "brand",
      "active-background": "background-weak",
      "active-text": "text-strong",
      "selected-background": "background-strong",
      "selected-text": "text-strong",
      "status-critical": "#FF4040",
      "status-warning": "#FFAA15",
      "status-ok": "#00C781",
      "status-unknown": "#CCCCCC",
      "status-disabled": "#CCCCCC"
    },
    font: {
      family: "helvetica neue"
    },
    graph: {
      colors: {
        dark: ["brand"],
        light: ["brand"]
      }
    }
  },
  heading: {
    font: {
      family: "helvetica neue"
    }
  }
};

export default {
  CLIENT_ID: CLIENT_ID,
  SCOPE: SCOPE,
  THEME: THEME,
  FOLDER_MIME_TYPE: FOLDER_MIME_TYPE
};
