import React from "react";
import { GoogleLogout } from "react-google-login";
import { Box, Text, Button } from "grommet";
import { CLIENT_ID } from "./Constants";

export default ({ handleLogout, handleFailure, name, imageUrl }) => {
  return (
    <Box
      direction="row"
      justify="end"
      align="center"
      gap="small"
      pad={{ horizontal: "small", vertical: "xsmall" }}
    >
      <Text>{name}</Text>
      <Box
        height="32px"
        width="32px"
        round="large"
        background={"url(" + imageUrl + ")"}
      ></Box>
      <Box>
        <GoogleLogout
          clientId={CLIENT_ID}
          onLogoutSuccess={handleLogout}
          onFailure={handleFailure}
          render={renderProps => (
            <Button
              label="Sign out"
              plain
              onClick={renderProps.onClick}
              disabled={renderProps.disabled}
            />
          )}
        />
      </Box>
    </Box>
  );
};
