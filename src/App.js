import React, { useState } from "react";
import { Grommet } from "grommet";
import LandingPage from "./LandingPage";
import { THEME } from "./Constants";
import { MainPage } from "./MainPage";

function App() {
  const [isLoggedIn, setLoggedIn] = useState(false);
  const [profileObj, setProfileObj] = useState({});
  const [accessToken, setAccessToken] = useState(null);

  const handleLogin = function(response) {
    setLoggedIn(true);
    setProfileObj(response.profileObj);
    setAccessToken(response.accessToken);
  };

  const handleLogout = function(response) {
    setLoggedIn(false);
  };

  const handleFailure = function(response) {
    alert(JSON.stringify(response));
  };

  if (!isLoggedIn) {
    return (
      <Grommet theme={THEME}>
        <LandingPage handleLogin={handleLogin} handleFailure={handleFailure} />
      </Grommet>
    );
  }

  return (
    <Grommet theme={THEME}>
      <MainPage
        handleLogout={handleLogout}
        handleFailure={handleFailure}
        profileObj={profileObj}
        accessToken={accessToken}
      />
    </Grommet>
  );
}

export default App;
