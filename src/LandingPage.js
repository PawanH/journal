import React from "react";
import { Box, Heading, Text } from "grommet";
import { GoogleLogin } from "react-google-login";
import { CLIENT_ID, SCOPE } from "./Constants";

export default ({ handleLogin, handleFailure }) => (
  <Box height="100vh" width="100vw">
    <Heading margin={{ vertical: "xlarge" }} alignSelf="center">
      Journal
    </Heading>
    <Box alignSelf="center">
      <Heading level={3} margin="none">
        Why use?
      </Heading>
      <li>
        <Text>
          Open-source. Free. I'm never going to make any money out of this.
        </Text>
      </li>
      <li>
        <Text>
          You own your data. All entries go straight to your own Google Drive
        </Text>
      </li>
      <li>
        <Text>
          Transparent. All API calls are made from your browser directly to
          Google APIs.
        </Text>
      </li>
    </Box>

    <Box width="fit-content" alignSelf="center" margin="medium">
      <GoogleLogin
        clientId={CLIENT_ID}
        onSuccess={handleLogin}
        onFailure={handleFailure}
        buttonText="Authorise with Google"
        isSignedIn={true}
        theme="dark"
        scope={SCOPE}
      />
    </Box>
  </Box>
);
