import { API_KEY, FOLDER_MIME_TYPE } from "./Constants";

export const createFileIfNotExist = async function(
  content,
  properties,
  slugs,
  mimeType,
  accessToken
) {
  let possibleParents = ["root"];
  for (let i = 0; i < slugs.length; i++) {
    const fileName = slugs[i];
    const currentMimeType = i < slugs.length - 1 ? FOLDER_MIME_TYPE : mimeType;

    const foundFiles = await findFiles(
      fileName,
      possibleParents,
      currentMimeType,
      accessToken
    );

    // The happy case
    // We found the common ancestor. Now create files
    if (foundFiles.length === 0 || i === slugs.length - 1) {
      if (possibleParents.length > 1) {
        const path = slugs.join("/");
        console.error(
          `More than one possible parent for the given path: ${path}`
        );
      }

      const parentId = possibleParents[0];

      return await createFile(
        content,
        properties,
        slugs.slice(i, slugs.length),
        parentId,
        mimeType,
        accessToken
      );
    }

    possibleParents = foundFiles;
  }
};

const createFile = async function(
  content,
  properties,
  slugs,
  parentId,
  mimeType,
  accessToken
) {
  for (let i = 0; i < slugs.length; i++) {
    const mime = i < slugs.length - 1 ? FOLDER_MIME_TYPE : mimeType;

    let data = {
      name: slugs[i],
      mimeType: mime,
      folderColorRgb: "red",
      parents: [parentId],
      properties: {
        journal: true
      }
    };

    const response = await fetch(
      `https://www.googleapis.com/drive/v3/files?key=${API_KEY}`,
      {
        headers: {
          Authorization: `Bearer ${accessToken}`,
          "Content-Type": "application/json"
        },
        method: "POST",
        body: JSON.stringify(data)
      }
    ).then(response => response.json());

    parentId = response.id;
  }

  // Now update the content
  return fetch(
    `https://www.googleapis.com/upload/drive/v3/files/${parentId}?key=${API_KEY}&uploadType=media`,
    {
      headers: {
        Authorization: `Bearer ${accessToken}`,
        "Content-Type": "text/markdown"
      },
      method: "PATCH",
      body: convertToMarkdown(content, properties)
    }
  )
    .then(response => response.json())
    .then(responseJson => responseJson.id);
};

function convertToMarkdown(content, properties) {
  return [
    `---`,
    ...Object.keys(properties).map(key => `${key}: ${properties[key]}`),
    `---`,
    content
  ].join("\n");
}

export function findFiles(fileName, possibleParents, mimeType, accessToken) {
  const parentsQueryComponent = possibleParents
    .map(parent => `"${parent}" in parents`)
    .join(" or ");

  const q = encodeURIComponent(
    `name="${fileName}" and trashed=false and mimeType="${mimeType}" and (${parentsQueryComponent})`
  );

  const query = `https://www.googleapis.com/drive/v3/files?corpus=user&q=${q}&key='${API_KEY}'&orderBy=createdTime desc`;

  return fetch(query, {
    headers: {
      Authorization: `Bearer ${accessToken}`,
      "Content-Type": "application/json"
    },
    method: "GET"
  })
    .then(response => response.json())
    .then(responseJson => responseJson.files.map(file => file.id));
}
