import React, { useState } from "react";
import Header from "./Header";
import { AuthorArea } from "./AuthorArea";
import { Box } from "grommet";
import { createFileIfNotExist } from "./GDriveHelper";

export const MainPage = ({
  handleFailure,
  handleLogout,
  profileObj,
  accessToken
}) => {
  const [coords, setCoords] = useState({});
  const [content, setContent] = useState("");

  const handlePositionUpdate = function(position) {
    setCoords(position.coords);
  };

  const handleTextChange = event => setContent(event.target.value);
  const handleReset = event => setContent("");

  const handlePublish = function(
    content,
    title,
    labels,
    { latitude, longitude }
  ) {
    const now = new Date();

    createFileIfNotExist(
      content,
      {
        local_time: now.toString(),
        unix_timestamp: now.getTime(),
        latitude: latitude,
        longitude: longitude
      },
      getJournalEntryPathsForNewEntry(title, labels, now),
      "text/markdown",
      accessToken
    )
      .then(id => alert("Successfully submitted entry"))
      .then(handleReset);
  };

  const getJournalEntryPathsForNewEntry = function(title, labels, now) {
    return [
      "journal",
      now.getUTCFullYear(),
      now.getUTCMonth() + 1,
      title ? `${title}.md` : `${now.getTime()}.md`
    ];
  };

  if (navigator.geolocation) {
    navigator.geolocation.watchPosition(handlePositionUpdate);
  }

  return (
    <Box height="100vh" width="100vw">
      <Header
        handleLogout={handleLogout}
        handleFailure={handleFailure}
        name={profileObj.name}
        imageUrl={profileObj.imageUrl}
      ></Header>
      <AuthorArea
        coords={coords}
        time={new Date()}
        handlePublish={handlePublish}
        handleTextChange={handleTextChange}
        handleReset={handleReset}
        content={content}
      />
    </Box>
  );
};
